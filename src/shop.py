class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        price_of_all_products = 0
        for product in self.products:
            price_of_all_products += product.get_price()
        return price_of_all_products

    def get_total_quantity_of_products(self):
        quantity_of_all_products = 0
        for product in self.products:
            quantity_of_all_products += product.quantity
        return quantity_of_all_products

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity
